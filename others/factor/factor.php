<?php

/**
 * Si es primo retorna 1 sino retorna el menor factor
 */
function isPrime($n) {
    for($i =2; $i < $n; $i++) {
        if($n % $i === 0) {
            return $i;
        }
    }
    return 1;
}

for ($i=0; $i < 4; $i++) { 
    $file = fopen("input/input".$i.".txt", "r");
    $fptr = fopen("output/output".$i.".txt", "w");
    
    $n = intval(trim(fgets($file)));

    $factor = isPrime($n);
    fwrite($fptr, $factor . "\n");

    fclose($file);
    fclose($fptr);
}