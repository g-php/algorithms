<?php

/**
 * Concatena:
 *  el string mas pequeño vocal-consonante 
 *  el string mas largo vocal-cualquier cosa-consonante
 */
function substring($s) {
    $vowels = ['a', 'e', 'i', 'o', 'u'];
    $consonants = ['b', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'x', 'y', 'z'];
    return smallest($s, $vowels, $consonants).PHP_EOL.largest($s, $vowels, $consonants);
}

/**
 * Encuentra la primera secuencia vocal-consonante
 */
function smallest($s, $vowels, $consonants) {
    for ($i=0; $i < strlen($s) - 1; $i++) { 
        if (in_array($s[$i], $vowels) && in_array($s[$i + 1], $consonants)) {
            return $s[$i].$s[$i + 1];
        }
    }
    return "VACIO";
}

function largest($s, $vowels, $consonants) {
    $posVowel = firstVowel($s, $vowels);
    $posConsonant = lastConsonant($s, $consonants);
    if ($posVowel !== -1 && $posConsonant !== -1 && $posVowel < $posConsonant) {
        return substr($s, $posVowel, $posConsonant - $posVowel + 1);
    } else {
        return "VACIO";
    }
}

/**
 * Encuentra la primera vocal
 */
function firstVowel($s, $vowels) {
    for ($i=0; $i < strlen($s) - 1; $i++) { 
        if (in_array($s[$i], $vowels)) {
            return $i;
        }
    }
    return -1;
}

/**
 * Encuentra la ultima consonante
 */
function lastConsonant($s, $consonants) {
    for ($i=strlen($s) - 1; $i > 0; $i--) { 
        if (in_array($s[$i], $consonants)) {
            return $i;
        }
    }
    return -1;
}


for ($i=0; $i < 4; $i++) { 
    $file = fopen("input/input".$i.".txt", "r");
    $fptr = fopen("output/output".$i.".txt", "w");
    
    $s = trim(fgets($file));

    $substring = substring($s);
    fwrite($fptr, $substring . "\n");

    fclose($file);
    fclose($fptr);
}