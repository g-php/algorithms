<?php

function gemStone($n) {
    $first = array_unique(str_split($n[0]));
    $count = 0;
    foreach ($first as $value) {
        for ($i=1; $i < sizeof($n); $i++) { 
            $isGem = true;
            if (strpos($n[$i], $value) === false) {
                $isGem = false;
                break;
            }
        }
        if ($isGem) {
            $count++;
        }
    }
    return $count;
}

for ($i=0; $i < 4; $i++) { 
    $file = fopen("input/input".$i.".txt", "r");
    $fptr = fopen("output/output".$i.".txt", "w");
    
    $rocks = explode(' ', rtrim(fgets($file)));
    $gemStone = gemStone($rocks);
    fwrite($fptr, $gemStone . "\n");

    fclose($file);
    fclose($fptr);
}