<?php

// Complete the plusMinus function below.
function plusMinus($arr) {
    $sizeArr =sizeof($arr);
    $positives = 0;
    $negatives = 0;
    $zeros = 0;
    foreach ($arr as $value) {
        switch (true) {
            case $value == 0:
                $zeros++;
                break;            
            case $value > 0:
                $positives++;
                break;
            case $value < 0:
                $negatives++;
                break;
        }
    }
    printf("%.6f\n",$positives/$sizeArr); 
    printf("%.6f\n",$negatives/$sizeArr); 
    printf("%.6f\n",$zeros/$sizeArr); 
}

$stdin = fopen("php://stdin", "r");

fscanf($stdin, "%d\n", $n);

fscanf($stdin, "%[^\n]", $arr_temp);

$arr = array_map('intval', preg_split('/ /', $arr_temp, -1, PREG_SPLIT_NO_EMPTY));

plusMinus($arr);

fclose($stdin);
