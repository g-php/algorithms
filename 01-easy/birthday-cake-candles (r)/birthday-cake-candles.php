<?php

// Complete the birthdayCakeCandles function below.
function birthdayCakeCandles($ar) {
    sort($ar);
    // array_search — Busca un valor determinado en un array y devuelve la primera clave correspondiente en caso de éxito
    // max — Encontrar el valor más alto
    $index = array_search(max($ar), $ar);
    // array_slice — Extraer una parte de un array
    // count — Cuenta todos los elementos de un array o algo de un objeto
    return count(array_slice($ar, $index));
}

/*****************************************************************/
for ($i=0; $i < 2; $i++) { 
    $file = fopen("input/input".$i.".txt", "r");
    $fptr = fopen("output/output".$i.".txt", "w");

    // Fue incluido sin sentido porque no se le pasa como parametro a la funcion
    $ar_count = intval(trim(fgets(STDIN)));

    $ar_temp = rtrim(fgets($file));
    $ar = array_map('intval', preg_split('/ /', $ar_temp, -1, PREG_SPLIT_NO_EMPTY));

    $result = birthdayCakeCandles($ar);

    fwrite($fptr, $result . "\n");
    fclose($fptr);
    fclose($file);
}