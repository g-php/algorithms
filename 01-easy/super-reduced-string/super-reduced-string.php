<?php

// Complete the superReducedString function below.
function superReducedString($s) {
    for ($i=0; $i < strlen($s) - 1; $i++) { 
        if ($s[$i] == $s[$i+1]) {
            $s = substr_replace($s, '', $i, 2);
            $i--;
            if ($i >= 0) {
                $i--;
            }
        }
    }
    if (strlen(trim($s)) == 0) {
        $s = "Empty String";
    }
    return $s;
}

for ($i=0; $i < 4; $i++) { 
    $i = 2;

    $fptr = fopen("output/output".$i.".txt", "w");

    $s = file_get_contents("input/input".$i.".txt");
    $result = superReducedString($s);
    var_dump($result);
    fwrite($fptr, $result . "\n");
    
    fclose($fptr);
    break;
}
