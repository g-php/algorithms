<?php

// Complete the divisibleSumPairs function below.
function divisibleSumPairs($n, $k, $ar) {
    $count = 0;
    for ($i=0; $i < sizeof($ar)-1; $i++) { 
        for ($j=$i+1; $j < sizeof($ar); $j++) { 
            if((($ar[$i] + $ar[$j]) % $k) == 0) {
                $count++;
            }
        }
    }
    return $count;
}

for ($i=0; $i < 2; $i++) { 
    $fptr = fopen("output/output".$i.".txt", "w");

    $stdin = fopen("input/input".$i.".txt", "r");
    
    fscanf($stdin, "%[^\n]", $nk_temp);
    $nk = explode(' ', $nk_temp);
    
    $n = intval($nk[0]);
    
    $k = intval($nk[1]);
    
    fscanf($stdin, "%[^\n]", $ar_temp);
    
    $ar = array_map('intval', preg_split('/ /', $ar_temp, -1, PREG_SPLIT_NO_EMPTY));
    
    $result = divisibleSumPairs($n, $k, $ar);
    
    fwrite($fptr, $result . "\n");
    
    fclose($stdin);
    fclose($fptr);
}
