<?php

/*
 * Complete the getMoneySpent function below.
 */
function getMoneySpent($keyboards, $drives, $b) {
    $max = -1;
    for ($i=0; $i < sizeof($keyboards); $i++) { 
        for ($j=0; $j < sizeof($drives); $j++) { 
            $combination = $keyboards[$i] + $drives[$j];
            if ($combination <= $b && $combination > $max) {
                $max = $combination;
            }
        }
    }
    return $max;
}

for ($i=0; $i < 2; $i++) { 
    $file = fopen("input/input".$i.".txt", "r");
    $fptr = fopen("output/output".$i.".txt", "w");
    
    $bnm = explode(' ', rtrim(fgets($file)));
    $b = intval($bnm[0]);
    $n = intval($bnm[1]);
    $m = intval($bnm[2]);

    $keyboards_temp = rtrim(fgets($file));    
    $keyboards = array_map('intval', preg_split('/ /', $keyboards_temp, -1, PREG_SPLIT_NO_EMPTY));

    $drives_temp = rtrim(fgets($file));    
    $drives = array_map('intval', preg_split('/ /', $drives_temp, -1, PREG_SPLIT_NO_EMPTY));

    $moneySpent = getMoneySpent($keyboards, $drives, $b);    
    fwrite($fptr, $moneySpent . "\n");

    fclose($file);
    fclose($fptr);
}