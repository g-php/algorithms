<?php

// Complete the solve function below.
function birthday($s, $d, $m) {
    $response = 0;
    for ($i=0; $i < sizeof($s); $i++) {
        $subarray = array_slice($s, $i, $m);
        if (array_sum($subarray) === $d) {
            $response++;
        }
    }
    return $response;
}

for ($i=0; $i < 3; $i++) { 
    $file = fopen("input/input" . $i . ".txt", "r");
    $fptr = fopen("output/output" . $i . ".txt", "w");

    $n = intval(trim(fgets($file)));


    $s_temp = rtrim(fgets($file));
    
    $s = array_map('intval', preg_split('/ /', $s_temp, -1, PREG_SPLIT_NO_EMPTY));

    $dm = explode(' ', rtrim(fgets($file)));
    // intval — Obtiene el valor entero de una variable
    $d = intval($dm[0]);    
    $m = intval($dm[1]);

    $result = birthday($s, $d, $m);

    fwrite($fptr, $result . "\n");

    fclose($file);
    fclose($fptr);
}