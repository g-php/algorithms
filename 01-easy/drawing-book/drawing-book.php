<?php

/*
 * Complete the pageCount function below.
 */
function pageCount($n, $p) {
    $front = intval($p/2);
    $back = intval($n/2) - $front;
    return min($front, $back);
}

for ($i=0; $i < 3; $i++) { 
    $fptr = fopen("output/output".$i.".txt", "w");

    $stdin = fopen("input/input".$i.".txt", "r");
    
    fscanf($stdin, "%d\n", $n);
    
    fscanf($stdin, "%d\n", $p);
    
    $result = pageCount($n, $p);
    var_dump($result);
    fwrite($fptr, $result . "\n");
    
    fclose($stdin);
    fclose($fptr);    
}
