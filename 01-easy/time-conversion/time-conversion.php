<?php

/*
 * Complete the timeConversion function below.
 */
function timeConversion($s) {
    $period = substr($s, -2);
    $hour = substr($s, 0,-2);
    $arr = explode(":", $hour);
    if ($period == "AM" && $arr[0] == "12") {
            $arr[0] = "00";
    }
    if ($period == "PM" && $arr[0] != "12") {
            $arr[0] = (int)$arr[0] + 12;
    }
    return implode(":", $arr);
}

$fptr = fopen(getenv("OUTPUT_PATH"), "w");

$__fp = fopen("php://stdin", "r");

fscanf($__fp, "%[^\n]", $s);

$result = timeConversion($s);
echo $result;
fwrite($fptr, $result . "\n");

fclose($__fp);
fclose($fptr);
