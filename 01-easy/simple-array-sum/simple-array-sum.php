<?php

define("OUTPUT_PATH", "/home/arnaldo/Documents/cursos/php/algorithms/02-simple-array-sum/file.txt");

/*
 * Complete the simpleArraySum function below.
 */
function simpleArraySum($ar) {
    $acum = 0;
    foreach ($ar as &$valor) {
        $acum += $valor;
    }
    echo "ACUM: ".$acum;
    return $acum;
}

// fopen — Abre un fichero
// getenv — Obtiene el valor de una variable de entorno
$fptr = fopen(getenv("OUTPUT_PATH"), "w");

$stdin = fopen("php://stdin", "r");

// fscanf — Analiza la entrada desde un archivo de acuerdo a un formato 
fscanf($stdin, "%d\n", $ar_count);

fscanf($stdin, "%[^\n]", $ar_temp);

// array_map — Aplica la retrollamada a los elementos de los arrays dados
// intval — Obtiene el valor entero de una variable
// preg_split — Divide un string mediante una expresión regular
// limit: Si se especifica, son devueltos únicamente los substrings hasta limit, 
// con el resto del string colocado en el último substring. 
// Si limit vale -1, 0 o NULL, significa "sin límite" y, como es un estándar en PHP, 
// se puede usar NULL para saltar hacia el parámetro flags.
// PREG_SPLIT_NO_EMPTY: Si se aplica esta bandera, solo serán devueltos los elementos
// no vacíos por preg_split().
$ar = array_map('intval', preg_split('/ /', $ar_temp, -1, PREG_SPLIT_NO_EMPTY));

$result = simpleArraySum($ar);

fwrite($fptr, $result . "\n");

fclose($stdin);
fclose($fptr);