<?php

function betweenRange($s, $t, $value) {
    return $value >= $s && $value <= $t;
}

function countFruits($fruits, $s, $t, $tree) {
    $countFruits = 0;
    foreach ($fruits as  $value) {
        $countFruits += betweenRange($s, $t, $tree + $value)? 1 : 0;    
    }
    return $countFruits;
}

// Complete the countApplesAndOranges function below.
function countApplesAndOranges($s, $t, $a, $b, $apples, $oranges) {
    $response =  countFruits($apples, $s, $t, $a).PHP_EOL;
    $response = $response . countFruits($oranges, $s, $t, $b);
    return $response;
}

/*****************************************************************/
for ($i=0; $i < 1; $i++) { 
    $file = fopen("input/input".$i.".txt", "r");
    $fptr = fopen("output/output".$i.".txt", "w");

    $st = explode(' ', rtrim(fgets($file)));
    $s = intval($st[0]);
    $t = intval($st[1]);
    
    $ab = explode(' ', rtrim(fgets($file)));
    $a = intval($ab[0]);
    $b = intval($ab[1]);

    $mn = explode(' ', rtrim(fgets($file)));
    $m = intval($mn[0]);
    $n = intval($mn[1]);

    $apples_temp = rtrim(fgets($file));
    $apples = array_map('intval', preg_split('/ /', $apples_temp, -1, PREG_SPLIT_NO_EMPTY));

    $oranges_temp = rtrim(fgets($file));
    $oranges = array_map('intval', preg_split('/ /', $oranges_temp, -1, PREG_SPLIT_NO_EMPTY));

    $total = countApplesAndOranges($s, $t, $a, $b, $apples, $oranges);
    fwrite($fptr, $total . "\n");
    fclose($fptr);
    fclose($file);
}
