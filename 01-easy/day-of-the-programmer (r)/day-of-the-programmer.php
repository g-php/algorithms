<?php

// Complete the dayOfProgrammer function below.
function dayOfProgrammer($year) {
    $months = [31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    $months[1] = getTotalFebruary($year);
    $dayMonth = getDayMonth($months);
    $date = new DateTime();
    $date->setDate($year, $dayMonth[1], $dayMonth[0]);
    return $date->format('d.m.Y');
}

function getTotalFebruary($year) {
    if ( in_array($year, range(1700, 1917)) ) {
        return isLeapJulian($year) ? 29 : 28;
    }
    if ( $year === 1918) {
        // cantidad de dias del mes de febrero del año particular: ultimo dia del mes - el primer dia del mes + 1
        return isLeapGregorian($year) ? 29 - 14 + 1 : 28 - 14 + 1;
    }
    if ( in_array($year, range(1919, 2700)) ) {
        return isLeapGregorian($year) ? 29 : 28;
    }
}

function getDayMonth($months) {
    $sum = 0;
    $date = [];
    for ($i=0; $i < sizeof($months); $i++) { 
        $sum += $months[$i];
        $date[0] = 256 - $sum; 
        if ($i > 1 && 256 - $sum < $months[$i + 1]) {
            $date[1]=$i + 2;
            return $date;
        }
    }
}

function isLeapJulian($year) {
    return $year % 4 === 0;
}

function isLeapGregorian($year) {
    $conditionOne = $year % 400 === 0;
    $conditionTwo = ($year % 4 === 0) && ($year % 100 !== 0);
    return ($conditionOne || $conditionTwo);
}

for ($i=0; $i < 3; $i++) { 
    $file = fopen("input/input".$i.".txt", "r");
    $fptr = fopen("output/output".$i.".txt", "w");

    $year = intval(trim(fgets($file)));
    $result = dayOfProgrammer($year);
    echo $result.PHP_EOL;
    fwrite($fptr, $result . "\n");
    fclose($fptr);
    fclose($file);
}