<?php

// Complete the migratoryBirds function below.
function migratoryBirds($ar) {
    $types = array("1"=>0, "2"=>0, "3"=>0, "4"=>0, "5"=>0);
    foreach ($ar as $value) {
        $types[$value]++;   
    }
    $arrHighest = array_keys($types, max($types));
    return current($arrHighest);
}

for ($i=0; $i < 4; $i++) { 
    $fptr = fopen("output/output".$i.".txt", "w");

    $stdin = fopen("input/input".$i.".txt", "r");
    
    fscanf($stdin, "%d\n", $ar_count);
    
    fscanf($stdin, "%[^\n]", $ar_temp);
    
    $ar = array_map('intval', preg_split('/ /', $ar_temp, -1, PREG_SPLIT_NO_EMPTY));
    
    $result = migratoryBirds($ar);
    
    fwrite($fptr, $result . "\n");
    
    fclose($stdin);
    fclose($fptr);    
}

