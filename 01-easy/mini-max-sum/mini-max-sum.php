<?php

// Complete the miniMaxSum function below.
function miniMaxSum($arr) {
    asort($arr);
    $min = array_sum(array_slice($arr, 0, 4));
    $max = array_sum(array_slice($arr, 1));
    echo $min." ".$max;
}

$stdin = fopen("php://stdin", "r");

fscanf($stdin, "%[^\n]", $arr_temp);

$arr = array_map('intval', preg_split('/ /', $arr_temp, -1, PREG_SPLIT_NO_EMPTY));

miniMaxSum($arr);

fclose($stdin);