<?php

// Complete the bonAppetit function below.
function bonAppetit($bill, $k, $b) {
    $rightPay = (array_sum($bill) - $bill[$k]) / 2; 
    if ( $rightPay === $b) {
        return "Bon Appetit";
        // echo "Bon Appetit"; // Asi en la versión original
    } else {
        return $b - $rightPay; 
        // echo $b - $rightPay; // Asi en la versión original
    }
}

for ($i=0; $i < 2; $i++) { 
    // fopen — Abre un fichero o un URL
    $file = fopen("input/input".$i.".txt", "r");
    $fptr = fopen("output/output".$i.".txt", "w");
    // fgets — Obtiene una línea desde el puntero a un fichero
    // explode — Divide un string en varios string. Devuelve un array de string
    $nk = explode(' ', rtrim(fgets($file)));
    // intval — Obtiene el valor entero de una variable
    $n = intval($nk[0]);    
    $k = intval($nk[1]);
    
    $bill_temp = rtrim(fgets($file));
    
    $bill = array_map('intval', preg_split('/ /', $bill_temp, -1, PREG_SPLIT_NO_EMPTY));
    
    $b = intval(trim(fgets($file)));
    
    $result = bonAppetit($bill, $k, $b);

    fwrite($fptr, $result . "\n");
    fclose($fptr);
    fclose($file);
}
