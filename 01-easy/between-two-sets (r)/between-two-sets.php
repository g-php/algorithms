<?php

/*
 * Complete the getTotalX function below.
 */
function getTotalX($a, $b) {
    $response = 0;
    $lcm = lcm_multiple($a);
    $gcd = gcd_multiple($b);
    $i = $lcm;
    while ($i <= $gcd) { 
        $response +=  $gcd % $i == 0 ? 1 : 0;
        $i += $lcm;
    }
    return $response;
}

/**
 * Least common multiple for multiples values
 */
function lcm_multiple($a) {
    $response = $a[0];
    for ($i=1; $i < sizeof($a); $i++) { 
        $response = lcm_pair($response, $a[$i]);
    }
    return $response;
}

/**
 * Least common multiple for two values
 */
function lcm_pair($a, $b) {
    $gcd = 0;
    $gcd = gcd_pair($a, $b);
    if ($gcd !== 0) {
        return ($a * $b) / $gcd;
    } else {
        return 0;
    }
}

/**
 * Greatest Common Divisor for multiples values
 */
function gcd_multiple($a) {
    $response = $a[0];
    for ($i=1; $i < sizeof($a); $i++) { 
        $response = gcd_pair($response, $a[$i]);
    }
    return $response;
}

/**
 * Greatest Common Divisor for two values
 */
function gcd_pair($a, $b) {
    $r = 0;
    if ($b !== 0) {
        $r = $a % $b;
    } else {
        return 0;
    }
    if ($r === 0 ) {
        return $b;
    } else {
        return gcd_pair($b, $r);
    }
}

/*****************************************************************/
for ($i=0; $i < 2; $i++) { 
    $file = fopen("input/input".$i.".txt", "r");
    $fptr = fopen("output/output".$i.".txt", "w");

    $nm = explode(' ', rtrim(fgets($file)));
    // intval — Obtiene el valor entero de una variable
    $n = intval($nm[0]);
    $m = intval($nm[1]);

    $a_temp = rtrim(fgets($file));
    $a = array_map('intval', preg_split('/ /', $a_temp, -1, PREG_SPLIT_NO_EMPTY));


    $b_temp = rtrim(fgets($file));
    $b = array_map('intval', preg_split('/ /', $b_temp, -1, PREG_SPLIT_NO_EMPTY));

    $total = getTotalX($a, $b);
    fwrite($fptr, $total . "\n");
    fclose($fptr);
    fclose($file);
}