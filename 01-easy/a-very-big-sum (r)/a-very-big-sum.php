<?php

// Complete the aVeryBigSum function below.
function aVeryBigSum($ar) {
    $acum = 0;
    foreach ($ar as $valor) {
        $acum += $valor;
    }
    return $acum;
}

for ($i=0; $i < 1; $i++) { 
    $file = fopen("input/input".$i.".txt", "r");
    $fptr = fopen("output/output".$i.".txt", "w");

    $ar_count = intval(rtrim(fgets($file)));
    
    $ar_temp = rtrim(fgets($file));
    
    $ar = array_map('intval', preg_split('/ /', $ar_temp, -1, PREG_SPLIT_NO_EMPTY));
    
    $result = aVeryBigSum($ar);

    fwrite($fptr, $result . "\n");
    fclose($fptr);
    fclose($file);
}