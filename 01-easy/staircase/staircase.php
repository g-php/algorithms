<?php

// Complete the staircase function below.
function staircase($n) {
    $space = $n;
    for ($i=1; $i <= $n; $i++) { 
        $arr = array_fill(0, $i, "#");
        $row = implode("",$arr);
        $arrSpace = array_fill(0, $space, "");
        $rowComplete = implode(" ",$arrSpace);
        $rowComplete .= $row;
        echo $rowComplete."\n";
        $space--; 
    }
}

$stdin = fopen("php://stdin", "r");

fscanf($stdin, "%d\n", $n);

staircase($n);

fclose($stdin);
