<?php

// Complete the hurdleRace function below.
function hurdleRace($k, $height) {
    $diff = max($height) - $k;
    return  $diff > 0 ? $diff : 0;
}

for ($i=0; $i < 2; $i++) { 
    $file = fopen("input/input".$i.".txt", "r");
    $fptr = fopen("output/output".$i.".txt", "w");

    $nk = explode(' ', rtrim(fgets($file)));
    $n = intval($nk[0]);
    $k = intval($nk[1]);

    $height_temp = rtrim(fgets($file));
    $height = array_map('intval', preg_split('/ /', $height_temp, -1, PREG_SPLIT_NO_EMPTY));

    $result = hurdleRace($k, $height);

    echo $result.PHP_EOL;
    fwrite($fptr, $result . "\n");
    fclose($fptr);
    fclose($file);
}