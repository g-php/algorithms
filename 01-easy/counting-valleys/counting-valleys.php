<?php

// Complete the countingValleys function below.
function countingValleys($n, $s) {
    $count = 0;
    $steps = 0;
    foreach (str_split($s) as  $value) {
        if ($steps == -1 && $value == "U") {
            $count++;
        }
        $steps += ($value == "U") ? 1 : -1;
    }
    return $count;
}

for ($i=0; $i < 2; $i++) { 
    $fptr = fopen("output/output".$i.".txt", "w");

    $stdin = fopen("input/input".$i.".txt", "r");
    
    fscanf($stdin, "%d\n", $n);
    
    $s = '';
    fscanf($stdin, "%[^\n]", $s);
    
    $result = countingValleys($n, $s);
    var_dump($result);
    fwrite($fptr, $result . "\n");
    
    fclose($stdin);
    fclose($fptr);    
}
