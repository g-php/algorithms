<?php

// Complete the catAndMouse function below.
function catAndMouse($x, $y, $z) {
    $diff = abs($x - $z) - abs($y - $z);
    switch (true) {
        case $diff < 0:
            return "Cat A";
            break;
        case $diff > 0:
            return "Cat B";
            break;
        default:
            return "Mouse C";
            break;
    }
}

for ($i=0; $i < 1; $i++) { 
    $fptr = fopen("output/output".$i.".txt", "w");

    $stdin = fopen("input/input".$i.".txt", "r");
    
    fscanf($stdin, "%d\n", $q);
    
    for ($q_itr = 0; $q_itr < $q; $q_itr++) {
        fscanf($stdin, "%[^\n]", $xyz_temp);
        $xyz = explode(' ', $xyz_temp);
    
        $x = intval($xyz[0]);
    
        $y = intval($xyz[1]);
    
        $z = intval($xyz[2]);
    
        $result = catAndMouse($x, $y, $z);
        echo $result."\n";
        fwrite($fptr, $result . "\n");
    }
    
    fclose($stdin);
    fclose($fptr);
}
