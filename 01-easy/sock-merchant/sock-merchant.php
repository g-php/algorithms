<?php

// Complete the sockMerchant function below.
function sockMerchant($n, $ar) {
    $count = 0;
    $colorPairsOfSocks = array();
    foreach ($ar as $value) {
        $colorPairsOfSocks[(string)$value] = array_key_exists((string)$value, $colorPairsOfSocks) ? $colorPairsOfSocks[(string)$value] + 1 : 1;
        if ($colorPairsOfSocks[(string)$value] == 2) {
           $count++;
           $colorPairsOfSocks[(string)$value] = 0;
        }
    }
    return $count;
}

for ($i=0; $i < 2; $i++) { 
    $fptr = fopen("output/output".$i.".txt", "w");

    $stdin = fopen("input/input".$i.".txt", "r");
    
    fscanf($stdin, "%d\n", $n);
    
    fscanf($stdin, "%[^\n]", $ar_temp);
    
    $ar = array_map('intval', preg_split('/ /', $ar_temp, -1, PREG_SPLIT_NO_EMPTY));
    
    $result = sockMerchant($n, $ar);
    
    fwrite($fptr, $result . "\n");
    
    fclose($stdin);
    fclose($fptr);    
}
