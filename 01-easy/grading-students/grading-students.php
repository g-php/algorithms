<?php

function nextMultiple($number, $multiple) {
    return ceil($number/$multiple) * $multiple;
}

/*
 * Complete the gradingStudents function below.
 */
function gradingStudents($grades) {
    $output = array();
    foreach ($grades as $value) {
        if ($value >= 38) {
            $next = nextMultiple($value, 5);
            $value = ($next - $value < 3) ? $next : $value;
        }
        array_push($output, $value);
    }
    return $output;
}

$fptr = fopen(getenv("OUTPUT_PATH"), "w");

$__fp = fopen("php://stdin", "r");

fscanf($__fp, "%d\n", $n);

$grades = array();

for ($grades_itr = 0; $grades_itr < $n; $grades_itr++) {
    fscanf($__fp, "%d\n", $grades_item);
    $grades[] = $grades_item;
}

$result = gradingStudents($grades);
fwrite($fptr, implode("\n", $result) . "\n");

fclose($__fp);
fclose($fptr);
