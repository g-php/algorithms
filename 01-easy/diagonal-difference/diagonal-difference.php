<?php

// Complete the diagonalDifference function below.
function diagonalDifference($arr) {
    $forward = 0;
    $backward = 0;
    $indexForward = 0;
    $indexBackward = sizeof($arr) -1;
    foreach ($arr as $row) {
        $forward += $row[$indexForward];  
        $backward += $row[$indexBackward];  
        $indexForward++;
        $indexBackward--;
    }
    return abs($forward - $backward);
 }

$fptr = fopen(getenv("OUTPUT_PATH"), "w");

$stdin = fopen("php://stdin", "r");

fscanf($stdin, "%d\n", $n);

$arr = array();

for ($i = 0; $i < $n; $i++) {
    fscanf($stdin, "%[^\n]", $arr_temp);
    $arr[] = array_map('intval', preg_split('/ /', $arr_temp, -1, PREG_SPLIT_NO_EMPTY));
}
$result = diagonalDifference($arr);
echo "RESULT: ".$result."\n";
fwrite($fptr, $result . "\n");

fclose($stdin);
fclose($fptr);