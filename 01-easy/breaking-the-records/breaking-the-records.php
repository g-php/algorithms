<?php

// Complete the breakingRecords function below.
function breakingRecords($scores) {
    $highestScore = $scores[0];
    $lowestScore = $scores[0];
    $countHightest = 0;
    $countLowest = 0;
    foreach ($scores as  $value) {
        if ($value > $highestScore) {
            $countHightest++;
            $highestScore = $value;
        }

        if ($value < $lowestScore) {
            $countLowest++;
            $lowestScore = $value;
        }
    }
    return array($countHightest, $countLowest);
}

for ($i=0; $i < 5; $i++) { 
    $fptr = fopen("output/output" . $i . ".txt", "w");

    $stdin = fopen("input/input" . $i . ".txt", "r");
    
    fscanf($stdin, "%d\n", $n);
    
    fscanf($stdin, "%[^\n]", $scores_temp);
    
    $scores = array_map('intval', preg_split('/ /', $scores_temp, -1, PREG_SPLIT_NO_EMPTY));
    
    $result = breakingRecords($scores);
    fwrite($fptr, implode(" ", $result) . "\n");
    
    fclose($stdin);
    fclose($fptr);
}