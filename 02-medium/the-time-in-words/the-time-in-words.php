<?php

// Complete the timeInWords function below.
function timeInWords($h, $m) {
    $h = intval($h);
    $m = intval($m);

    $output = "";

    $words = array(
        1 => "one",
        2 => "two",
        3 => "three",
        4 => "four",
        5 => "five",
        6 => "six",
        7 => "seven",
        8 => "eight",
        9 => "nine",
        10 => "ten",
        11 => "eleven",
        12 => "twelve",
        13 => "thirteen",
        14 => "fourteen",
        15 => "quarter",
        16 => "sixteen",
        17 => "seventeen",
        18 => "eighteen",
        19 => "nineteen",
        20 => "twenty",
        30 => "half",
        40 => "forty",
        50 => "fifty"
    );

    switch (true) {
        case $m == 0:
            $output = $words[$h] . " o' clock";
            break;
        case $m == 1:
            $output = $words[$m] . " minute past " . $words[$h];
            break;
        case $m == 15:
        case $m == 30:
            $output = $words[$m] . " past " . $words[$h];
            break;      
        case $m <= 20:
            $output = $words[$m] . " minutes past " . $words[$h];
            break;      
        case $m > 20 && $m < 30:
            $splitMinutes = str_split($m);
            $tens = $splitMinutes[0]."0";
            $ones = $splitMinutes[1];
            $output = $words[$tens] . " " . $words[$ones] . " minutes past " . $words[$h];
            break;
        case $m > 30:
            $rest = 60 - $m;
            switch ($rest) {
                case 15:
                    $output = $words[$rest] . " to " . $words[$h + 1];
                    break;
                default:
                    if ($rest <= 20) {
                        $output = $words[$rest] . " minutes to " . $words[$h + 1];
                    } else {
                        $splitMinutes = str_split($rest);
                        $tens = $splitMinutes[0]."0";
                        $ones = $splitMinutes[1];
                        $output = $words[$tens] . " " . $words[$ones] . " minutes to " . $words[$h + 1];
                    }
                    break;
            }
            break;
    }
    return $output;
}

for ($i=0; $i < 11; $i++) { 
    $fptr = fopen("output/output".$i.".txt", "w");

    $stdin = fopen("input/input".$i.".txt", "r");
    
    fscanf($stdin, "%d\n", $h);
    
    fscanf($stdin, "%d\n", $m);
    
    $result = timeInWords($h, $m);
    echo $result."\n";
    fwrite($fptr, $result . "\n");
    
    fclose($stdin);
    fclose($fptr);    
}
