<?php

// Complete the encryption function below.
function encryption($s) {
    $sqrt = sqrt(strlen($s));
    $floor = floor($sqrt);
    $ceil = ceil($sqrt);
    echo "FLOOR: ".$floor."\n";
    echo "CEIL: ".$ceil."\n";
    $chunks = array_chunk(str_split($s), $ceil);
    $output = array();
    for ($i=0; $i <= $floor; $i++) { 
        array_push($output, implode("", array_column($chunks, $i)));
    }
    return implode(" ", $output);
}

for ($i=0; $i < 3; $i++) { 
    $fptr = fopen("output/output".$i.".txt", "w");

    $stdin = fopen("input/input".$i.".txt", "r");
    
    $s = '';
    fscanf($stdin, "%[^\n]", $s);
    
    $result = encryption($s);
    echo "RESULT: ".$result."\n";
    fwrite($fptr, $result . "\n");
    
    fclose($stdin);
    fclose($fptr);    
}

