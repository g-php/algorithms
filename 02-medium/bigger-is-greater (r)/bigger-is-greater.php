<?php

// Complete the biggerIsGreater function below.
function biggerIsGreater($w) {
    $array = str_split($w);
    $initial = initialDecreasingSuffix($array);
    if ($initial >= 0 && sizeof($array) > $initial) {
        
        // SORT END
        $end = array_slice($array, $initial + 1);
        sort($end);
        array_splice($array, $initial + 1, count($end), $end);
        
        // SWAP POSITION
        $swapPosition = getSwapPosition($array[$initial], $end);
        $swapPosition += $initial;

        // SWAP
        $temp = $array[$initial];
        $array[$initial] = $array[$swapPosition];
        $array[$swapPosition] = $temp;
        
        $w = implode($array);
        return $w;
    } else {
        return 'no answer';
    }

}

function initialDecreasingSuffix($s) {
    for ($i=sizeof($s) - 1; $i > 0; $i--) { 
        if ($i - 1 >= 0 && $s[$i - 1] < $s[$i]) {
            return $i - 1;
        }
    }
    return -1;
}

function getSwapPosition($value, $array) {
    for ($i=0; $i < sizeof($array); $i++) { 
        if ($value < $array[$i]) {
            return $i + 1;
        }
    }
}

for ($i=0; $i < 2; $i++) { 
    $file = fopen("input/input".$i.".txt", "r");
    $fptr = fopen("output/output".$i.".txt", "w");
    
    $n = intval(trim(fgets($file)));
    
    for ($T_itr = 0; $T_itr < $n; $T_itr++) {
        $w = rtrim(fgets($file));

        $result = biggerIsGreater($w);
        fwrite($fptr, $result . "\n");
    }
    
    fclose($file);
    fclose($fptr);
}
