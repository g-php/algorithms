<?php

// Complete the getMinimumCost function below.
function getMinimumCost($k, $c) {
    $total = 0;
    sort($c); 
    // var_dump($c);
    $k = array_fill(0, $k, 0);
    $pos = 0;


    for ($i = sizeof($c) - 1 ; $i >= 0; $i--) { 
        $pos = $pos % sizeof($k);
        $total += ($k[$pos] + 1) * $c[$i];
        echo 'POS: '.$pos.' K[$pos]'.$k[$pos].' c[$i] '.$c[$i].' TOTAL: '.$total. PHP_EOL;
        $k[$pos]++;
        $pos++;
    }
    return $total;
}

for ($i=0; $i < 6; $i++) { 
    $file = fopen("input/input".$i.".txt", "r");
    $fptr = fopen("output/output".$i.".txt", "w");
    
    $nk = explode(' ', rtrim(fgets($file)));
    $n = intval($nk[0]);
    $k = intval($nk[1]);
    
    $c_temp = rtrim(fgets($file));
    
    $c = array_map('intval', preg_split('/ /', $c_temp, -1, PREG_SPLIT_NO_EMPTY));

    $minimumCost = getMinimumCost($k, $c);
    echo $minimumCost. PHP_EOL;
    fwrite($fptr, $minimumCost . "\n");
    
    fclose($file);
    fclose($fptr);
}
