<?php

// Complete the extraLongFactorials function below.
function extraLongFactorials($n) {
    $result = 1;
    while ($n >= 1) {
        $result = bcmul($result, $n);
        $n--;
    }
    return $result;

}

for ($i=0; $i < 2; $i++) { 
    // fopen — Abre un fichero o un URL
    $file = fopen("input/input".$i.".txt", "r");
    $fptr = fopen("output/output".$i.".txt", "w");

    $n = intval(trim(fgets($file)));

    $result = extraLongFactorials($n);
    echo $result.PHP_EOL;
    fwrite($fptr, $result . "\n");
    fclose($fptr);
    fclose($file);


}