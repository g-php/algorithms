<?php

function distance($queen, $obstacle, $limit) {
    $output = 0;
    if ($obstacle != -1) {
        $output = abs($queen - $obstacle) - 1;
    } else {
        $output = abs($limit - $queen) - 1;
    }
    return $output;
}

function distanceDiagonal($queenRow, $queenColumn, $obstacle, $limitRow, $limitColumn) {
    $output = 0;
    if ($obstacle != -1) {
        $output = abs($obstacle - $queenRow) - 1;
    } else {
        $differenceRow = abs($limitRow - $queenRow);
        $differenceColumn = abs($limitColumn - $queenColumn);
        $output = ($differenceRow < $differenceColumn) ? $differenceRow : $differenceColumn; 
    }
    return $output;
}

// Complete the queensAttack function below.
function queensAttack($n, $k, $r_q, $c_q, $obstacles) {
    $firstObstacle = array("horizontalLeft" => -1, "horizontalRight" => -1, "verticalDown" => -1, "verticalUp" => -1, "backwardDown" => -1, "backwardUp" => -1, "forwardDown" => -1, "forwardUp" => -1);
    $output = array("horizontalLeft" => 0, "horizontalRight" => 0, "verticalDown" => 0, "verticalUp" => 0, "backwardDown" => 0, "backwardUp" => 0, "forwardDown" => 0, "forwardUp" => 0);

    foreach ($obstacles as $r_c) {
        // HORIZONTAL - obstaculo en la fila
        if ($r_q == $r_c[0]) {
            // Primer obstaculo vertical inferior - se establecen por la fila (row)
            if ($c_q > $r_c[1] && ($firstObstacle['horizontalLeft'] == -1 || $r_c[1] > $firstObstacle['horizontalLeft'])) {
                $firstObstacle['horizontalLeft'] = $r_c[1];
            } else { // Primer obstaculo vertical superior
                if ($c_q < $r_c[1] && ($firstObstacle['horizontalRight'] == -1 || $r_c[1] < $firstObstacle['horizontalRight'])) {
                    $firstObstacle['horizontalRight'] = $r_c[1];
                }
            }
        }
        // VERTICAL - obstaculo en la columna
        if ($c_q == $r_c[1]) {
            // Primer obstaculo vertical inferior - se establecen por la fila (row)
            if ($r_q > $r_c[0] && ($firstObstacle['verticalDown'] == -1 || $r_c[0] > $firstObstacle['verticalDown'])) {
                $firstObstacle['verticalDown'] = $r_c[0];
            } else { // Primer obstaculo vertical superior
                if ($r_q < $r_c[0] && ($firstObstacle['verticalUp'] == -1 || $r_c[0] < $firstObstacle['verticalUp'])) {
                    $firstObstacle['verticalUp'] = $r_c[0];
                }
            }
        }
       // DIAGONAL - Misma diferencia de filas que de columnas
        if (abs($r_q - $r_c[0]) == abs($c_q - $r_c[1])) {
            if ($r_c[0] < $r_q) { // DIAGONAL INFERIOR - compara por fila
                if ($r_c[1] < $c_q) { // OBSTACULO A LA IZQUIERDA - compara por columna
                    if ($firstObstacle['forwardDown'] == -1 || $r_c[0] > $firstObstacle['forwardDown']) { // compara por fila
                        $firstObstacle['forwardDown'] = $r_c[0];
                    }
                } else { // OBSTACULO A LA DERECHA
                    if ($firstObstacle['backwardDown'] == -1 || $r_c[0] > $firstObstacle['backwardDown']) {
                        $firstObstacle['backwardDown'] = $r_c[0];
                    }
                }
            } else { // DIAGONAL SUPERIOR - compara por fila
                if ($r_c[1] < $c_q) { // OBSTACULO A LA IZQUIERDA - por columna
                    if ($firstObstacle['backwardUp'] == -1 || $r_c[0] < $firstObstacle['backwardUp']) {
                        $firstObstacle['backwardUp'] = $r_c[0];
                    }
                } else { // OBSTACULO A LA DERECHA
                    if ($firstObstacle['forwardUp'] == -1 || $r_c[0] < $firstObstacle['forwardUp']) {
                        $firstObstacle['forwardUp'] = $r_c[0];
                    }
                }
            }
        }
    }
    echo "FIRST OBSTACLES\n";
    var_dump($firstObstacle);
    $output['horizontalLeft'] = distance($c_q, $firstObstacle['horizontalLeft'], 0);
    $output['horizontalRight'] = distance($c_q, $firstObstacle['horizontalRight'], $n + 1);
    
    $output['verticalDown'] = distance($r_q, $firstObstacle['verticalDown'], 0);
    $output['verticalUp'] = distance($r_q, $firstObstacle['verticalUp'], $n + 1);
    
    $output['backwardDown'] = distanceDiagonal($r_q, $c_q, $firstObstacle['backwardDown'], 1, $n);
    $output['backwardUp'] = distanceDiagonal($r_q, $c_q, $firstObstacle['backwardUp'], $n, 1);
    $output['forwardDown'] = distanceDiagonal($r_q, $c_q, $firstObstacle['forwardDown'], 1, 1);
    $output['forwardUp'] = distanceDiagonal($r_q, $c_q, $firstObstacle['forwardUp'], $n, $n);
    
    echo "ALCANCES POR DIRECCION\n";
    var_dump($output);

    return array_sum($output);
}

for ($j=0; $j < 6; $j++) { 
    echo "ITERACION: ".$j."\n";
    $fptr = fopen("output/output".$j.".txt", "w");

    $stdin = fopen("input/input".$j.".txt", "r");
    
    fscanf($stdin, "%[^\n]", $nk_temp);
    $nk = explode(' ', $nk_temp);
    
    $n = intval($nk[0]);
    
    $k = intval($nk[1]);
    
    fscanf($stdin, "%[^\n]", $r_qC_q_temp);
    $r_qC_q = explode(' ', $r_qC_q_temp);
    
    $r_q = intval($r_qC_q[0]);
    
    $c_q = intval($r_qC_q[1]);
    
    $obstacles = array();
    
    for ($i = 0; $i < $k; $i++) {
        fscanf($stdin, "%[^\n]", $obstacles_temp);
        $obstacles[] = array_map('intval', preg_split('/ /', $obstacles_temp, -1, PREG_SPLIT_NO_EMPTY));
    }
    
    $result = queensAttack($n, $k, $r_q, $c_q, $obstacles);
    var_dump($result);
    fwrite($fptr, $result . "\n");
    fclose($stdin);
    fclose($fptr);
    echo "***************************************************\n";    
}
