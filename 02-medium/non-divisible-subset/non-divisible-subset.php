<?php

// Complete the nonDivisibleSubset function below.
function nonDivisibleSubset($k, $S) {
    $subset = array();
    $operations = array();

    for ($i=0; $i < sizeof($S)-1; $i++) { 
        for ($j=$i + 1; $j < sizeof($S); $j++) {
            $remainder = (($S[$i] + $S[$j]) % $k == 0) ? true : false;
            array_push($operations, array($S[$i], $S[$j], $remainder));   
        }
    }
    foreach ($operations as $operation) {
        if (!$operation[2]) {
            if (!in_array($operation[0], $subset, true)) {
                array_push($subset, $operation[0]);
            }
            if (!in_array($operation[1], $subset, true)) {
                array_push($subset, $operation[1]);
            }
        } else {
            if (in_array($operation[1], $subset, true)) {
                $key = array_key_exists($operation[1], $subset);
                echo "KEY: ".$key." VALUE: ".$operation[1]."\n";
                unset($subset[$key]);
            }
        }
    }

    if ($k == 3) {
        var_dump($operations);
        echo "\n\n\n";
        var_dump($subset);
    }
    return sizeof($subset);
}

for ($i=0; $i < 3; $i++) { 
    $fptr = fopen("output/output".$i.".txt", "w");

    $stdin = fopen("input/input".$i.".txt", "r");
    
    fscanf($stdin, "%[^\n]", $nk_temp);
    $nk = explode(' ', $nk_temp);
    
    $n = intval($nk[0]);
    
    $k = intval($nk[1]);
    
    fscanf($stdin, "%[^\n]", $S_temp);
    
    $S = array_map('intval', preg_split('/ /', $S_temp, -1, PREG_SPLIT_NO_EMPTY));
    
    $result = nonDivisibleSubset($k, $S);
    fwrite($fptr, $result . "\n");
    
    fclose($stdin);
    fclose($fptr);        
}